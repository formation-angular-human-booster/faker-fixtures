<?php

namespace App\DataFixtures;

use App\Entity\Marque;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class MarqueFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
       $faker = Faker\Factory::create('fr_FR');
       $marques = [];
       for ($i = 0; $i<50; $i++) {
           $marqueToAdd = new Marque();
           $marqueToAdd->setName($faker->company);
           $manager->persist($marqueToAdd);
           $marques[] = $marqueToAdd;
       }

        $manager->flush();


    }
}
