<?php

namespace App\DataFixtures;

use App\Entity\Car;
use App\Entity\Marque;
use App\Repository\MarqueRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;
use const http\Client\Curl\Versions\CURL;

class CarFixtures extends Fixture implements DependentFixtureInterface
{
    private  $marqueRepository;

    public function __construct(MarqueRepository  $marqueRepository)
    {
        $this->marqueRepository = $marqueRepository;
    }

    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('fr_FR');

        $marques = $this->marqueRepository->findAll();


        for ($i = 0; $i<50; $i++) {
            $car = new Car();
            $car->setColor($faker->colorName);
            $car->setMarque($marques[$i% count($marques)]);
            $manager->persist($car);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MarqueFixtures::class
        ];
    }
}
